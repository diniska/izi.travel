//
//  IZITravelVkontakteAuthManagerTests.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ITTVkontakteAuthManager.h"

@interface IZITravelVkontakteAuthManagerTests : XCTestCase

@end

@interface ITTVkontakteAuthManagerDelegateMock : NSObject<ITTVkontakteAuthManagerDelegate>
@property (assign, nonatomic) BOOL isAuthManagerNeedToOpenWebView;
@end

@implementation IZITravelVkontakteAuthManagerTests
- (void)testAuthManagerOpenWebViewAfterAuthorizationStart {
    ITTVkontakteAuthManagerDelegateMock *mock = [[ITTVkontakteAuthManagerDelegateMock alloc] init];
    ITTVkontakteAuthManager *manager = [[ITTVkontakteAuthManager alloc] initWithDelegate:mock];
    [manager startAuthorization];
    XCTAssertTrue(mock.isAuthManagerNeedToOpenWebView, @"auth manager should ask to open web view");
}
@end

@implementation ITTVkontakteAuthManagerDelegateMock
- (void)authManager:(ITTVkontakteAuthManager *)manager needToOpenWebViewWithUrl:(NSString *)url webViewDelegate:(id<UIWebViewDelegate>)delegate {
    self.isAuthManagerNeedToOpenWebView = YES;
}

- (void)authManagerDidAuthorizeUser:(ITTVkontakteAuthManager *)manager {
    
}

- (void)authManagerDidFailUserAuthorization:(ITTVkontakteAuthManager *)manager
                                  withError:(NSError *)error
                            needToShowError:(BOOL)needToShowError {
    
}
@end

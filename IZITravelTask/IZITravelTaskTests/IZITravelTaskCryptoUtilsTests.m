//
//  IZITravelTaskCryptoUtilsTests.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//
#import <XCTest/XCTest.h>

#import "ITTCryptoUtils.h"

@interface IZITravelTaskCryptoUtilsTests : XCTestCase

@end

@implementation IZITravelTaskCryptoUtilsTests
- (void)testShouldCorrectlyComputeMd5OfString {
    NSString *const input = @"Hello world";
    NSString *const wantedOutput = @"3e25960a79dbc69b674cd4ec67a72c62";
    NSString *const output = [ITTCryptoUtils md5Hash:input];
    XCTAssertEqualObjects(output, wantedOutput, @"must be equals");
}

- (void)testShouldCorrectlyComputeVkontakteHashWithSortedParams {
    NSDictionary *const params = @{
                                   @"api_id" : @"4",
                                   @"method" : @"getFriends",
                                   @"v" : @"3.0"
                             };
    NSString *const viewerId = @"6492";
    NSString *const secret = @"secret";
    NSString *wantedOutput = @"ec88fbee10af1b28c41c5b2e85ae0502";
    NSString *output = [ITTCryptoUtils md5HashOfVkParameters:params viewerId:viewerId secret:secret];
    XCTAssertEqualObjects(output, wantedOutput, @"must be equals");
}

- (void)testShouldCorrectlyComputeVkontakteHashWithNotSortedParams {
    NSDictionary *const params = @{
                                   @"method" : @"getFriends",
                                   @"v" : @"3.0",
                                   @"api_id" : @"4",
                                   };
    NSString *const viewerId = @"6492";
    NSString *const secret = @"secret";
    NSString *wantedOutput = @"ec88fbee10af1b28c41c5b2e85ae0502";
    NSString *output = [ITTCryptoUtils md5HashOfVkParameters:params viewerId:viewerId secret:secret];
    XCTAssertEqualObjects(output, wantedOutput, @"must be equals");
}

- (void)testShoultCorrectlyComputeVkontakteHashWithoutViewerId {
    NSDictionary *const params = @{
                                   @"api_id" : @"4",
                                   @"method" : @"getFriends",
                                   @"v" : @"3.0"
                                   };
    NSString *const viewerId = nil;
    NSString *const secret = @"api_secret";
    NSString *wantedOutput = @"fbd3bc00725799ecd835bf661391ac34";
    NSString *output = [ITTCryptoUtils md5HashOfVkParameters:params viewerId:viewerId secret:secret];
    XCTAssertEqualObjects(output, wantedOutput, @"must be equals");
}
@end

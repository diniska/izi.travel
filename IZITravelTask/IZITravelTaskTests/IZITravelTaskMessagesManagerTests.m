//
//  IZITravelTaskMessagesManagerTests.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ITTMessageManager_Tests.h"
#import "ITTVkontakteMessage.h"

@interface IZITravelTaskMessagesManagerTests : XCTestCase

@end

@implementation IZITravelTaskMessagesManagerTests
- (void)testMessageManagerShouldReturnCorrectMessageNumber {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:23];
    for (int i = 0; i < 23; ++i) {
        [array addObject:[[ITTVkontakteMessage alloc] init]];
    }
    ITTMessageManager *manager = [[ITTMessageManager alloc] init];
    [manager setMessages:array];
    XCTAssertEqual(manager.messageCount, 23, @"must be equal");
    XCTAssertEqual(manager.numberOfReadenMessages, 0, @"must be equal");
}

- (void)testMessageManagerShouldReturnCorrectNumberOfReadenAfterRead5Messages {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:23];
    for (int i = 0; i < 23; ++i) {
        [array addObject:[[ITTVkontakteMessage alloc] init]];
    }
    ITTMessageManager *manager = [[ITTMessageManager alloc] init];
    [manager setMessages:array];
    for (int i = 0; i < 5; ++i) {
        [manager markMessageAtIndexAsRead:i];
    }
    XCTAssertEqual(manager.messageCount, 23, @"must be equal");
    XCTAssertEqual(manager.numberOfReadenMessages, 5, @"must be equal");
}

- (void)testMessageManagerShouldReturnCorrectMessageIndex {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:23];
    for (int i = 0; i < 23; ++i) {
        [array addObject:[[ITTVkontakteMessage alloc] init]];
    }
    ITTMessageManager *manager = [[ITTMessageManager alloc] init];
    [manager setMessages:array];
    for (int i = 0; i < manager.messageCount; ++i) {
        id message = [manager messageAtIndex:i];
        XCTAssertEqual([manager indexOfMessage:message], i, @"message index");
    }
}
@end

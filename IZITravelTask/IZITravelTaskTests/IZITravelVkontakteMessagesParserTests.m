//
//  IZITravelVkontakteMessagesParserTests.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "ITTVkontakteMessage+Server.h"

@interface IZITravelVkontakteMessagesParserTests : XCTestCase

@end

@implementation IZITravelVkontakteMessagesParserTests
- (void)testShouldCorrectlyParseDictionaryToVkontakteMessage {
    NSNumber *mid = @10;
    NSString *body = @"Hello world";
    NSString *readState = @"1";
    NSDictionary *input = @{
                            @"mid" : mid,
                            @"body" : body,
                            @"read_state" : readState
                            };
    ITTVkontakteMessage *message = [ITTVkontakteMessage messageWithServerDictionary:input];
    XCTAssertEqual([mid integerValue], message.serverId, @"must be equals");
    XCTAssertEqualObjects(body, message.text, @"must be equals");
    XCTAssertEqual([readState boolValue], message.isRead, @"must be equals");
}
@end

//
//  ITTMesssageViewController.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ITTMesssageViewController;
@class ITTVkontakteMessage;

@protocol ITTMesssageViewControllerDelegate <NSObject>
- (void)messageViewController:(ITTMesssageViewController *)controller didReadMessage:(ITTVkontakteMessage *)message;
@end

@interface ITTMesssageViewController : UIViewController
@property (strong, nonatomic) ITTVkontakteMessage *message;
@property (weak, nonatomic) id<ITTMesssageViewControllerDelegate> delegate;
@end

//
//  ITTMesssageViewController.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTMesssageViewController.h"
#import "ITTVkontakteMessage.h"
#import "ITTVkontakteServer.h"
#import "ITTAlertUtils.h"

@interface ITTMesssageViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
@end

@implementation ITTMesssageViewController {
    BOOL _viewVisible;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateTextView];
    self.textView.editable = NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _viewVisible = YES;
    [self markMessageAsReadAndNotifyDelegate];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _viewVisible = NO;
}

#pragma mark - Setters
- (void)setMessage:(ITTVkontakteMessage *)message {
    _message = message;
    [self updateTextView];
    if (_viewVisible) {
        [self markMessageAsReadAndNotifyDelegate];
    }
}

#pragma mark - Private
- (void)updateTextView {
    self.textView.text = self.message.text;
}

- (void)markMessageAsReadAndNotifyDelegate {
    if (!self.message.isRead) {
        __weak typeof(self) bself = self;
        [[ITTVkontakteServer sharedInstance] messagesMarkAsRead:[NSSet setWithObject: @(self.message.serverId)] callback:nil errorCallback:^(NSError *error) {
            if (bself) {
                [ITTAlertUtils showAlertAboutError:error];
            }
        }];
    }
    if (self.message) {
        [self.delegate messageViewController:self didReadMessage:self.message];
    }
}
@end

//
//  ITTViewController.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 26.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTViewController.h"
#import "ITTVkontakteAuthManager.h"
#import "ITTWebViewController.h"
#import "ITTMessageManager.h"
#import "ITTVkontakteServer.h"
#import "ITTAlertUtils.h"
#import "ITTMesssageViewController.h"
#import "ITTVkontakteMessage.h"

static NSString *const kShowAuthorizationWebViewSegueId = @"ShowAuthorizationWebView";
static NSString *const kShowTextViewSegueId = @"ShowTextView";

static inline UIFont * kTextFont() {
    return [UIFont systemFontOfSize:15];//TODO: integration with iOS 7 font size if need
}

static CGFloat const kTableViewCellTextIndentationWidth = 15;

@interface ITTViewController () <ITTVkontakteAuthManagerDelegate, ITTMessageManagerDelegate, ITTMesssageViewControllerDelegate>
@property (strong, nonatomic) ITTVkontakteAuthManager *authManager;
@property (strong, nonatomic) ITTMessageManager *messageManager;
@end

@implementation ITTViewController {
    id<UIWebViewDelegate> _authorizationWebViewDelegate;
    NSString *_authorizationUrlToLoadInWebView;
    UIRefreshControl *_refreshControl;
    ITTVkontakteMessage *_messageToShow;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateTitle];
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self
                       action:@selector(didPullToRefresh)
             forControlEvents:UIControlEventValueChanged];
    [self setRefreshControl:_refreshControl];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.authManager == nil) {
        self.authManager = [[ITTVkontakteAuthManager alloc] initWithDelegate:self];
        [self.authManager startAuthorization];
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kShowAuthorizationWebViewSegueId]) {
        ITTWebViewController *controller = segue.destinationViewController;
        controller.webViewDelegate = _authorizationWebViewDelegate;
        controller.urlToLoad = _authorizationUrlToLoadInWebView;
    } else if ([segue.identifier isEqualToString:kShowTextViewSegueId]) {
        ITTMesssageViewController *controller = segue.destinationViewController;
        controller.message = _messageToShow;
        controller.delegate = self;
    }
}

#pragma mark - ITTVkontakteAuthManagerDelegate
- (void)authManager:(ITTVkontakteAuthManager *)manager needToOpenWebViewWithUrl:(NSString *)url webViewDelegate:(id<UIWebViewDelegate>)delegate {
    _authorizationWebViewDelegate = delegate;
    _authorizationUrlToLoadInWebView = url;
    [self performSegueWithIdentifier:kShowAuthorizationWebViewSegueId sender:self];
}

- (void)authManagerDidAuthorizeUser:(ITTVkontakteAuthManager *)manager {
    [self dismissViewControllerAnimated:YES completion:nil];
    ITTVkontakteServer *server = [ITTVkontakteServer sharedInstance];
    server.accessToken = [self.authManager accessToken];
    ITTMessageManager *messageManager = [[ITTMessageManager alloc] init];
    messageManager.server = server;
    messageManager.delegate = self;
    self.messageManager = messageManager;
    [self.refreshControl beginRefreshing];
    [self loadData];
}

- (void)authManagerDidFailUserAuthorization:(ITTVkontakteAuthManager *)manager
                                  withError:(NSError *)error
                            needToShowError:(BOOL)needToShowError {
    if (needToShowError) {
        [ITTAlertUtils showAlertAboutError:error];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - User events
- (void)didPullToRefresh {
    [self loadData];
}

#pragma mark - Setters
- (void)setMessageManager:(ITTMessageManager *)messageManager {
    _messageManager = messageManager;
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
}


#pragma mark- ITTMessageManagerDelegate
- (void)messageManagerDidReloadMessages:(ITTMessageManager *)manager {
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
    [self updateTitle];
}

- (void)messageManagerDidFailLoadMessages:(ITTMessageManager *)manager withError:(NSError *)error {
    [ITTAlertUtils showAlertAboutError:error];
    [self.refreshControl endRefreshing];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.messageManager messageCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const kMessageCellReuseId = @"message";
    UITableViewCell *res = [tableView dequeueReusableCellWithIdentifier:kMessageCellReuseId];
    if (res == nil) {
        res = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMessageCellReuseId];
        res.textLabel.font = kTextFont();
        res.textLabel.numberOfLines = 0;
        res.indentationWidth = kTableViewCellTextIndentationWidth;
    }
    res.textLabel.text = [self textToDisplayInCellAtIndexPath:indexPath];
    return res;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGSize constraintSize = (CGSize){self.tableView.bounds.size.width - kTableViewCellTextIndentationWidth, CGFLOAT_MAX};
    CGSize labelSize = [[self textToDisplayInCellAtIndexPath:indexPath] sizeWithFont:kTextFont() constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping]; //TODO: use ios7 method if need
    return labelSize.height + 20;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _messageToShow = [self.messageManager messageAtIndex:indexPath.row];
    [self performSegueWithIdentifier:kShowTextViewSegueId sender:self];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.messageManager isMessageRead:indexPath.row]) {
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    }
}

#pragma mark - ITTMesssageViewControllerDelegate
- (void)messageViewController:(ITTMesssageViewController *)controller didReadMessage:(ITTVkontakteMessage *)message {
    NSInteger messageIndex = [self.messageManager indexOfMessage:message];
    [self.messageManager markMessageAtIndexAsRead:messageIndex];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:messageIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [self updateTitle];
}

#pragma mark - Private
- (void)loadData {
    [self.messageManager reloadAll];
}

- (NSString *)textToDisplayInCellAtIndexPath:(NSIndexPath *)indexPath {
    return [self.messageManager messageAtIndex:indexPath.row].text;
}

- (void)updateTitle {
    if (self.messageManager.messageCount == 0) {
        self.title = @"Messages";
    } else {
        self.title = [NSString stringWithFormat:@"Messages (%@)", @(self.messageManager.messageCount - self.messageManager.numberOfReadenMessages)];
    }
}
@end

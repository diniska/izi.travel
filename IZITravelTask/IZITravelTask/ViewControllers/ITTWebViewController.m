//
//  ITTWebViewController.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 27.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTWebViewController.h"

@interface ITTWebViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation ITTWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.webView.delegate = self.webViewDelegate;
    if (self.urlToLoad) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlToLoad]]];
    }
}

#pragma mark - Setters
- (void)setWebViewDelegate:(id<UIWebViewDelegate>)webViewDelegate {
    _webViewDelegate = webViewDelegate;
    self.webView.delegate = _webViewDelegate;
}

- (void)setUrlToLoad:(NSString *)urlToLoad {
    _urlToLoad = urlToLoad;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlToLoad]]];
}

@end

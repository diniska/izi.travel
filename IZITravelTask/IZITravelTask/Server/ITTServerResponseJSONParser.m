//
//  ITTServerResponseJSONParser.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 26.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTServerResponseJSONParser.h"

@implementation ITTServerResponseJSONParser
- (id)parseResponse:(NSData *)response error:(NSError *__autoreleasing *)error {
    id const res = [NSJSONSerialization JSONObjectWithData:response options:0 error:error];
    return res;
}
@end

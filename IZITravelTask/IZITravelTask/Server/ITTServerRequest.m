//
//  ITTServerRequest.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 26.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTServerRequest.h"
#import "ITTServerResponseParser.h"
#import "ITTNetworkIndicator.h"

NSString *const kITTServerRequestErrorDomain = @"network";
NSInteger const kITTServerRequestUnableToCreateConnectionErrorCode = 5;
NSString *const kITTServerRequestErrorTypeKey = @"type";
NSString *const kITTServerRequestErrorTypeNoInternetConnection = @"No connection";

@interface ITTServerRequest () <NSURLConnectionDataDelegate>
@end

@implementation ITTServerRequest {
    NSURLRequest *_request;
    BOOL _cancelled;
    NSURLConnection *_connection;
    NSMutableData *_responseData;
}

- (id)initWithRequest:(NSURLRequest *)request {
    if (self = [super init]) {
        _request = request;
    }
    return self;
}

- (void)start {
    if (_connection != nil) {
        return;
    }
    [[ITTNetworkIndicator sharedInstance] incrementNetworkActivity];
    _connection = [[NSURLConnection alloc] initWithRequest:_request delegate:self startImmediately:NO];
    if (_connection == nil) {
        NSDictionary *userInfo = @{
                                   kITTServerRequestErrorTypeKey : kITTServerRequestErrorTypeNoInternetConnection
                                   };
        NSError *error = [[NSError alloc] initWithDomain:kITTServerRequestErrorDomain
                                                    code:kITTServerRequestUnableToCreateConnectionErrorCode
                                                userInfo:userInfo];
        [self connection:_connection didFailWithError:error];
        
    } else {
        [_connection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        [_connection start];
        if ([self.delegate respondsToSelector:@selector(serverRequestDidStart:)]) {
            [self.delegate serverRequestDidStart:self];
        }
    }
}

- (void)cancel {
    if (_connection != nil) {
        return;
    }
    [_connection cancel];
    _connection = nil;
    if ([self.delegate respondsToSelector:@selector(serverRequestDidInterrupt:)]) {
        [self.delegate serverRequestDidInterrupt:self];
    }
    [[ITTNetworkIndicator sharedInstance] decrementNetworkActivity];
}

#pragma mark - NSURLConnectionDataDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    dispatch_async(self.queueToExecute, ^{
        NSError *error;
        id res = [self.responseParser parseResponse:_responseData error:&error];
        if (self.callback && error == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.callback(res);
            });
        }
        if (error) {
            [self connection:connection didFailWithError:error];
        } else if ([self.delegate respondsToSelector:@selector(serverRequestDidFinish:)]) {
            [self.delegate serverRequestDidFinish:self];
        }
        [[ITTNetworkIndicator sharedInstance] decrementNetworkActivity];
    });
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    dispatch_async(self.queueToExecute, ^{
        if (self.errorCallback) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.errorCallback(error);
            });
        }
        if ([self.delegate respondsToSelector:@selector(serverRequestDidFail:)]) {
            [self.delegate serverRequestDidFail:self];
        }
        [[ITTNetworkIndicator sharedInstance] decrementNetworkActivity];
    });
}

#pragma mark - Getters
- (dispatch_queue_t)queueToExecute {
    return _queueToExecute ?: dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
}
@end

//
//  ITTServerRequestExecutor_Protected.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 26.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTServerRequestExecutor.h"
#import "ITTServerRequest.h"

extern NSString *const kITTServerRequestErrorDomain;
extern NSInteger const kITTServerRequestUnableToCreateConnectionErrorCode;
extern NSString *const kITTServerRequestErrorTypeKey;
extern NSString *const kITTServerRequestErrorTypeNoInternetConnection;

@protocol  ITTServerResponseParser;

@interface ITTServerRequestExecutor ()
@property (strong, nonatomic) id<ITTServerResponseParser> responseParser;

- (void)performGETRequestWithUrl:(NSString *)url
                          params:(NSDictionary *)params
                        callback:(ITTServerRequestCallback)callback
                   errorCallback:(ITTServerRequestErrorCallback)errorCallback;

- (void)performPOSTRequestWithUrl:(NSString *)url
                           params:(NSDictionary *)params
                         callback:(ITTServerRequestCallback)callback
                    errorCallback:(ITTServerRequestErrorCallback)errorCallback;

+ (NSURL *)apiUrlWithUrl:(NSString *)url params:(NSDictionary *)params;
@end

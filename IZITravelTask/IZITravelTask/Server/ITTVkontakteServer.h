//
//  ITTVkontakteServer.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 27.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTServerRequestExecutor.h"
#import "ITTServerRequest.h"

extern NSString *const kVkontakteServerErrorDomain;

typedef NS_ENUM(int, ITTVkontakteServerMessageFilter) {
    ITTVkontakteServerMessageFilterNotReaded = 1,
    ITTVkontakteServerMessageFilterNotFromChat = 2,
    ITTVkontakteServerMessageFilterFromFriends = 4
};

@interface ITTVkontakteServer : ITTServerRequestExecutor
+ (ITTVkontakteServer *)sharedInstance;
@property (strong, nonatomic) NSString *accessToken;

- (void)messagesGetCount:(NSNumber *)numberOfMessages
                  filter:(NSNumber *)filter
                callback:(void (^)(NSInteger allMessagesNumber, NSArray *messages))callback
           errorCallback:(ITTServerRequestErrorCallback)errorCallback;

- (void)messagesMarkAsRead:(NSSet *)messageIds
                  callback:(void (^)(BOOL success))callback
             errorCallback:(ITTServerRequestErrorCallback)errorCallback;
@end

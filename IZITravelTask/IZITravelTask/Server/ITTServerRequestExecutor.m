//
//  ITTServerRequestExecutor.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 26.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTServerRequestExecutor.h"
#import "ITTServerRequestExecutor_Protected.h"
#import "ITTServerRequest.h"

@interface ITTServerRequestExecutor() <ITTServerRequestDelegate>
@end

@implementation ITTServerRequestExecutor {
    NSMutableSet *_requests;
    dispatch_queue_t _requestsQueue;
}
- (id)init {
    if (self = [super init]) {
        _requests = [NSMutableSet set];
        _requestsQueue = dispatch_queue_create("ITTServerRequestExecutor queue", DISPATCH_QUEUE_CONCURRENT);
        dispatch_set_target_queue(_requestsQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
    }
    return self;
}

#pragma mark - Protected

- (void)performGETRequestWithUrl:(NSString *)urlString
                          params:(NSDictionary *)params
                        callback:(ITTServerRequestCallback)callback
                   errorCallback:(ITTServerRequestErrorCallback)errorCallback {
    dispatch_async(_requestsQueue, ^{
        NSURL *const url = [[self class] apiUrlWithUrl:urlString params:params];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        ITTServerRequest *const request = [self createServerRequestWithUrlRequest:urlRequest
                                                                         callback:callback
                                                                    errorCallback:errorCallback];
        [_requests addObject:request];
        [request start];
    });
}

- (void)performPOSTRequestWithUrl:(NSString *)urlString
                           params:(NSDictionary *)params
                         callback:(ITTServerRequestCallback)callback
                    errorCallback:(ITTServerRequestErrorCallback)errorCallback {
    dispatch_async(_requestsQueue, ^{
        NSURL *const url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
        [urlRequest setHTTPMethod:@"POST"];
        NSData *const data = [[[self class] createRequestParametersString:params] dataUsingEncoding:NSUTF8StringEncoding];
        [urlRequest setHTTPBody:data];
        ITTServerRequest *const request = [self createServerRequestWithUrlRequest:urlRequest
                                                                         callback:callback
                                                                    errorCallback:errorCallback];
        [_requests addObject:request];
        [request start];
    });
}

+ (NSURL *)apiUrlWithUrl:(NSString *)url params:(NSDictionary *)params {
    NSString *const resultUrl = [url stringByAppendingFormat:@"?%@",
                                 [self createRequestParametersString:params]];
    return [NSURL URLWithString:resultUrl];
}

+ (NSString *)createRequestParametersString:(NSDictionary *)params {
    NSMutableArray *const pairs = [NSMutableArray array];
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [pairs addObject:[NSString stringWithFormat:@"%@=%@", key, obj]];
    }];
    NSString *const paramsString = [pairs componentsJoinedByString:@"&"];
    return paramsString;
}

#pragma mark - ITTServerRequestDelegate
- (void)serverRequestDidInterrupt:(ITTServerRequest *)request {
    [self removeRequest:request];
}

- (void)serverRequestDidFinish:(ITTServerRequest *)request {
    [self removeRequest:request];
}

- (void)serverRequestDidFail:(ITTServerRequest *)request {
    [self removeRequest:request];
}

#pragma mark - Private
- (void)removeRequest:(ITTServerRequest *)request {
    [_requests removeObject:request];
}

- (ITTServerRequest *)createServerRequestWithUrlRequest:(NSURLRequest *)urlRequest
                                               callback:(id)callback
                                          errorCallback:(id)errorCallback{
    ITTServerRequest *const request = [[ITTServerRequest alloc] initWithRequest:urlRequest];
    request.queueToExecute = _requestsQueue;
    request.callback = callback;
    request.errorCallback = errorCallback;
    request.responseParser = self.responseParser;
    request.delegate = self;
    return request;
}
@end

//
//  ITTServerResponseParser.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 26.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ITTServerResponseParser <NSObject>
- (id)parseResponse:(NSData *)response error:(NSError *__autoreleasing *)error;
@end

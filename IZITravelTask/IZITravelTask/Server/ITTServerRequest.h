//
//  ITTServerRequest.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 26.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kITTServerRequestErrorDomain;
extern NSInteger const kITTServerRequestUnableToCreateConnectionErrorCode;
extern NSString *const kITTServerRequestErrorTypeKey;
extern NSString *const kITTServerRequestErrorTypeNoInternetConnection;

typedef void (^ITTServerRequestCallback)(id result);
typedef void (^ITTServerRequestErrorCallback)(NSError *error);

@class ITTServerRequest;
@protocol ITTServerResponseParser;

@protocol ITTServerRequestDelegate <NSObject>
@optional
- (void)serverRequestDidStart:(ITTServerRequest *)request;
- (void)serverRequestDidInterrupt:(ITTServerRequest *)request;
- (void)serverRequestDidFinish:(ITTServerRequest *)request;
- (void)serverRequestDidFail:(ITTServerRequest *)request;
@end

/**
 * Need to store object during request
 */
@interface ITTServerRequest : NSObject
- (id)initWithRequest:(NSURLRequest *)request;
- (void)start;
- (void)cancel;

@property (assign, nonatomic) dispatch_queue_t queueToExecute;
@property (weak, nonatomic) id<ITTServerRequestDelegate> delegate;
@property (strong, nonatomic) id<ITTServerResponseParser> responseParser;
@property (copy, nonatomic) ITTServerRequestCallback callback;
@property (copy, nonatomic) ITTServerRequestErrorCallback errorCallback;
@end

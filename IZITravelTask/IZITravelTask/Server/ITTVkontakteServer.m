//
//  ITTVkontakteServer.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 27.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTVkontakteServer.h"
#import "ITTServerRequestExecutor_Protected.h"
#import "ITTServerResponseJSONParser.h"
#import "ITTVkontakteMessage+Server.h"

static NSString *const kApiUrl = @"https://api.vk.com/method/";
static NSString *const kVkApiMethodGetMessages = @"messages.get";
static NSString *const kVkApiMethodMessagesMarkAsRead = @"messages.markAsRead";

NSString *const kVkontakteServerErrorDomain = @"vkontakte";

@implementation ITTVkontakteServer
+ (ITTVkontakteServer *)sharedInstance {
    static ITTVkontakteServer *res;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        res = [[ITTVkontakteServer alloc] init];
    });
    return res;
}

- (id)init {
    if (self = [super init]) {
        self.responseParser = [[ITTServerResponseJSONParser alloc] init];
    }
    return self;
}

- (void)messagesGetCount:(NSNumber *)numberOfMessages
                  filter:(NSNumber *)filter
                callback:(void (^)(NSInteger allMessagesNumber, NSArray *messages))callback
           errorCallback:(ITTServerRequestErrorCallback)errorCallback {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:numberOfMessages forKey:@"count"];
    [params setValue:filter forKey:@"filters"];
    [self signReuqestParameters:params];
    NSString *url = [self urlForMethod:kVkApiMethodGetMessages];
    [self performGETRequestWithUrl:url params:params callback:^(NSDictionary *result) {
        if (![self processErrorIfNeed:result toErrorCallback:errorCallback]) {
            NSArray *response = result[@"response"];
            NSInteger allMessageNumber = [response[0] integerValue];
            NSArray *messages;
            if (response.count > 1){
                messages = [response subarrayWithRange:NSMakeRange(1, response.count - 1)];
            }
            if (callback) {
                callback(allMessageNumber, [ITTVkontakteMessage messagesWithServerArray:messages]);
            }
        }
    } errorCallback:errorCallback];
}

- (void)messagesMarkAsRead:(NSSet *)messageIds
                  callback:(void (^)(BOOL success))callback
             errorCallback:(ITTServerRequestErrorCallback)errorCallback {
    NSMutableDictionary *const params = [@{
                                           @"mids" : [[messageIds allObjects] componentsJoinedByString:@","]
                                           } mutableCopy];
    [self signReuqestParameters:params];
    NSString *const url = [self urlForMethod:kVkApiMethodMessagesMarkAsRead];
    [self performGETRequestWithUrl:url params:params callback:^(NSDictionary *result) {
        if (![self processErrorIfNeed:result toErrorCallback:errorCallback]) {
            if (callback) {
                callback([result[@"response"] integerValue]);
            }
        }
    } errorCallback:errorCallback];
}

#pragma mark - Private
- (void)signReuqestParameters:(NSMutableDictionary *)parameters {
    parameters[@"access_token"] = self.accessToken;
}

- (BOOL)processErrorIfNeed:(NSDictionary *)result toErrorCallback:(ITTServerRequestErrorCallback)errorCallback {
    NSDictionary *errorDictionary = result[@"error"];
    BOOL needToProcess = NO;
    if (errorDictionary) {
        needToProcess = YES;
        NSInteger const code = [errorDictionary[@"error_code"] integerValue];
        NSError *const error = [[NSError alloc] initWithDomain:kVkontakteServerErrorDomain code:code userInfo:errorDictionary];
        if (errorCallback) {
            errorCallback(error);
        }
        DLog(@"error processed %@", error);
    }
    return needToProcess;
}

- (NSString *)urlForMethod:(NSString *)method {
   return [kApiUrl stringByAppendingString:method];
}
@end

//
//  ITTServerResponseJSONParser.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 26.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ITTServerResponseParser.h"

@interface ITTServerResponseJSONParser : NSObject <ITTServerResponseParser>
@end

//
//  ITTMessageManager.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ITTVkontakteServer;
@class ITTMessageManager;
@class ITTVkontakteMessage;

@protocol ITTMessageManagerDelegate <NSObject>
@optional
- (void)messageManagerDidReloadMessages:(ITTMessageManager *)manager;
- (void)messageManagerDidFailLoadMessages:(ITTMessageManager *)manager withError:(NSError *)error;
@end

@interface ITTMessageManager : NSObject

@property (strong, nonatomic) ITTVkontakteServer *server;
@property (weak, nonatomic) id<ITTMessageManagerDelegate> delegate;

- (NSInteger)messageCount;
- (NSUInteger)numberOfReadenMessages;

- (ITTVkontakteMessage *)messageAtIndex:(NSUInteger)index;
- (NSInteger)indexOfMessage:(ITTVkontakteMessage *)message;

- (void)reloadAll;

- (void)markMessageAtIndexAsRead:(NSInteger)messageIndex;
- (BOOL)isMessageRead:(NSInteger)index;
@end

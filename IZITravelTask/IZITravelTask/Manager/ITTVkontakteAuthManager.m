//
//  ITTVkontakteAuthManager.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 27.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTVkontakteAuthManager.h"

static NSString *const kAppId = @"4384397";
static NSString *const kRedirectUrl = @"blank.html";

@interface ITTVkontakteAuthManager () <UIWebViewDelegate>
@property (copy, nonatomic, readwrite) NSString *accessToken;
@end

@implementation ITTVkontakteAuthManager {
    __weak id<ITTVkontakteAuthManagerDelegate> _delegate;
    NSString *_accessToken;
}
- (id)initWithDelegate:(id<ITTVkontakteAuthManagerDelegate>)delegate {
    if (self = [super init]) {
        _delegate = delegate;
    }
    return self;
}

- (void)startAuthorization {
    NSString *url = [NSString stringWithFormat:@"https://oauth.vk.com/authorize?client_id=%@&scope=messages&redirect_uri=%@&display=touch&response_type=token", kAppId, kRedirectUrl];
    [_delegate authManager:self needToOpenWebViewWithUrl:url webViewDelegate:self];
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *url = [request.URL absoluteString];
    if ([url rangeOfString:@"access_token="].location != NSNotFound) {
        [self webViewDidReceiveTokenInUrl:url];
        return  NO;
    } else if ([url rangeOfString:@"error"].location != NSNotFound) {
        [self didFailtToReceiveAccessToken:url];
        return NO;
    } else {
        return YES;
    }
}

- (void)webViewDidReceiveTokenInUrl:(NSString *)url {
    NSArray *urlParts = [url componentsSeparatedByString:@"#"];
    if (urlParts.count < 2) {
        [self didFailWithParsingAccessToken];
        return;
    }
    
    NSString *keysAndValues = urlParts[1];
    NSArray *keysAndValuesArray = [keysAndValues componentsSeparatedByString:@"&"];
    NSString *accessToken;
    for (NSString *kv in keysAndValuesArray) {
        if ([kv rangeOfString:@"access_token"].location != NSNotFound) {
            NSArray *parts = [kv componentsSeparatedByString:@"="];
            if (parts.count < 2) {
                [self didFailWithParsingAccessToken];
                return;
            }
            accessToken = parts[1];
            break;
        }
    }
    [self didReceiveAccessToken:accessToken];
}

- (void)didFailtToReceiveAccessToken:(NSString *)url {
    [_delegate authManagerDidFailUserAuthorization:self withError:nil needToShowError:NO];//TODO: get error from url
}

- (void)didFailWithParsingAccessToken {
    [_delegate authManagerDidFailUserAuthorization:self withError:nil needToShowError:NO];//TODO: create parsing error with description
}

- (void)didReceiveAccessToken:(NSString *)accessToken {
    if (accessToken == nil) {
        [_delegate authManagerDidFailUserAuthorization:self withError:nil needToShowError:YES];
    } else {
        self.accessToken = accessToken;
        [_delegate authManagerDidAuthorizeUser:self];
    }
}

@end

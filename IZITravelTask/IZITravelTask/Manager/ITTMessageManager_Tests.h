//
//  ITTMessageManager_Tests.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTMessageManager.h"

@interface ITTMessageManager ()
- (void)setMessages:(NSArray *)messages;
@end

//
//  ITTVkontakteAuthManager.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 27.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ITTVkontakteAuthManager;

@protocol ITTVkontakteAuthManagerDelegate <NSObject>
- (void)authManager:(ITTVkontakteAuthManager *)manager needToOpenWebViewWithUrl:(NSString *)url webViewDelegate:(id<UIWebViewDelegate>)delegate;
- (void)authManagerDidAuthorizeUser:(ITTVkontakteAuthManager *)manager;
- (void)authManagerDidFailUserAuthorization:(ITTVkontakteAuthManager *)manager
                                  withError:(NSError *)error
                            needToShowError:(BOOL)needToShowError;
@end

@interface ITTVkontakteAuthManager : NSObject
- (id)initWithDelegate:(id<ITTVkontakteAuthManagerDelegate>)delegate;
- (void)startAuthorization;
@property (copy, nonatomic, readonly) NSString *accessToken;
@end

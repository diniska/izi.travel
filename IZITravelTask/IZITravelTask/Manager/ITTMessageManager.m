//
//  ITTMessageManager.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTMessageManager.h"
#import "ITTVkontakteServer.h"

@implementation ITTMessageManager {
    NSArray *_messages;
    NSMutableIndexSet *_readMessagesIds;
}

- (NSInteger)messageCount {
    return _messages.count;
}

- (NSDictionary *)messageAtIndex:(NSUInteger)index {
    return _messages[index];
}

- (void)reloadAll {
    __weak typeof(self) bself = self;
    [self.server messagesGetCount:@20 filter:@(ITTVkontakteServerMessageFilterFromFriends) callback:^(NSInteger allMessagesNumber, NSArray *messages) {
        [bself setMessages:messages];
    } errorCallback:^(NSError *error) {
        [bself didFailWithMessageReloading:error];
    }];
}

- (void)markMessageAtIndexAsRead:(NSInteger)messageIndex {
    [_readMessagesIds addIndex:messageIndex];
}

- (NSInteger)indexOfMessage:(ITTVkontakteMessage *)message {
    return [_messages indexOfObject:message];
}

- (BOOL)isMessageRead:(NSInteger)index {
    return [_readMessagesIds containsIndex:index];
}

- (void)setMessages:(NSArray *)messages {
    _messages = messages;
    _readMessagesIds = [[NSMutableIndexSet alloc] init];
    if ([self.delegate respondsToSelector:@selector(messageManagerDidReloadMessages:)]) {
        [self.delegate messageManagerDidReloadMessages:self];
    }
}

- (void)didFailWithMessageReloading:(NSError *)error {
    if ([self.delegate respondsToSelector:@selector(messageManagerDidFailLoadMessages:withError:)]) {
        [self.delegate messageManagerDidFailLoadMessages:self withError:error];
    }
}

- (NSUInteger)numberOfReadenMessages {
    return _readMessagesIds.count;
}
@end

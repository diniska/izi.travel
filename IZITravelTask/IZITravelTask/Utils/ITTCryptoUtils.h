//
//  ITTCryptoUtils.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITTCryptoUtils : NSObject
+ (NSString *)md5Hash:(NSString *)input;

#pragma mark - Vkontakte
+ (NSString *)md5HashOfVkParameters:(NSDictionary *)parameters
                           viewerId:(NSString *)viewerId
                             secret:(NSString *)secret;
@end

//
//  ITTNetworkIndicator.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTNetworkIndicator.h"

static NSTimeInterval const kDelayToUpdateNetworkIndicatorState = .5;

@implementation ITTNetworkIndicator {
    NSLock *_indicatorCounterLock;
    NSInteger _networkActivities;
}
+ (instancetype)sharedInstance {
    static ITTNetworkIndicator *res;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        res = [[ITTNetworkIndicator alloc] init];
    });
    return res;
}

- (id)init {
    if (self = [super init]) {
        _indicatorCounterLock = [[NSLock alloc] init];
    }
    return self;
}

- (void)incrementNetworkActivity {
    [_indicatorCounterLock lock];
    ++_networkActivities;
    [_indicatorCounterLock unlock];
    [self setNeedsUpdateIndicatorState];
}

- (void)decrementNetworkActivity {
    [_indicatorCounterLock lock];
    --_networkActivities;
    [_indicatorCounterLock unlock];
    [self setNeedsUpdateIndicatorState];
}

- (void)setNeedsUpdateIndicatorState {
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(updateIndicatorState)
                                               object:nil];
    [self performSelector:@selector(updateIndicatorState)
                   withObject:nil
                   afterDelay:kDelayToUpdateNetworkIndicatorState];
}

- (void)updateIndicatorState {
    NSInteger networkActivities;
    [_indicatorCounterLock lock];
    networkActivities = _networkActivities;
    [_indicatorCounterLock unlock];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:networkActivities > 0];
    });
}
@end

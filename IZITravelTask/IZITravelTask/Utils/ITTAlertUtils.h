//
//  ITTAlertUtils.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITTAlertUtils : NSObject
+ (void)showAlertAboutError:(NSError *)error;
@end

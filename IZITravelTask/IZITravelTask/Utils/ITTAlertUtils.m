//
//  ITTAlertUtils.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTAlertUtils.h"

@implementation ITTAlertUtils
+ (void)showAlertAboutError:(NSError *)error {
    UIAlertView *const alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}
@end

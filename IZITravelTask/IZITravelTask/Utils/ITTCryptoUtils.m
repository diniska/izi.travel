//
//  ITTCryptoUtils.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>

#import "ITTCryptoUtils.h"

@implementation ITTCryptoUtils
+ (NSString *)md5Hash:(NSString *)input {
    // Create pointer to the string as UTF8
    const char *ptr = [input UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(ptr, strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

+ (NSString *)md5HashOfVkParameters:(NSDictionary *)parameters
                           viewerId:(NSString *)viewerId
                             secret:(NSString *)secret {
    NSArray *keys = [[parameters allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSMutableString *toComputeHash = [NSMutableString string];
    if (viewerId) {
        [toComputeHash appendString:viewerId];
    }
    for (NSString *k in keys) {
        [toComputeHash appendFormat:@"%@=%@", k, parameters[k]];
    }
    if (secret) {
        [toComputeHash appendString:secret];
    }
    return [self md5Hash:toComputeHash];
}
@end

//
//  ITTNetworkIndicator.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITTNetworkIndicator : NSObject
+ (instancetype)sharedInstance;
- (void)incrementNetworkActivity;
- (void)decrementNetworkActivity;
@end

//
//  main.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 26.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ITTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ITTAppDelegate class]));
    }
}

//
//  ITTVkontakteMessage+Server.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTVkontakteMessage.h"

@interface ITTVkontakteMessage (Server)
+ (instancetype)messageWithServerDictionary:(NSDictionary *)dictionary;
+ (NSArray *)messagesWithServerArray:(NSArray *)serverArray;
@end

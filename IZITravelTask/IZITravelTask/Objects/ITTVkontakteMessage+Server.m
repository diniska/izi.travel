//
//  ITTVkontakteMessage+Server.m
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "ITTVkontakteMessage+Server.h"

static NSString *const kMessageId = @"mid";
static NSString *const kText = @"body";
static NSString *const kReadState = @"read_state";

@implementation ITTVkontakteMessage (Server)
+ (instancetype)messageWithServerDictionary:(NSDictionary *)dictionary {
    ITTVkontakteMessage *res = [[ITTVkontakteMessage alloc] init];
    res.serverId = [dictionary[kMessageId] integerValue];
    res.text = dictionary[kText];
    res.read = [dictionary[kReadState] boolValue];
    return res;
}

+ (NSArray *)messagesWithServerArray:(NSArray *)serverArray {
    NSMutableArray *res = [NSMutableArray arrayWithCapacity:serverArray.count];
    [serverArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        [res addObject:[self messageWithServerDictionary:obj]];
    }];
    return res;
}
@end

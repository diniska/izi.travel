//
//  ITTVkontakteMessage.h
//  IZITravelTask
//
//  Created by Denis Chaschin on 28.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITTVkontakteMessage : NSObject
@property (assign, nonatomic) NSInteger serverId;
@property (strong, nonatomic) NSString *text;
@property (assign, nonatomic, getter = isRead) BOOL read;
@end
